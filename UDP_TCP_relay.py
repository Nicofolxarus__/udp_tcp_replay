#!/usr/bin/env python3

import argparse
import time
import socket
from threading import Thread
import datetime
import sys

def recvSegTCP(socket):
    size = min(int.from_bytes(socket.recv(4), 'big'), 1024)
    #print("RECIVE SIZE {}".format(size))
    return socket.recv(size)


def sendSegTCP(socket, data):
    #print("SEND SIZE {}".format(sys.getsizeof(data)))
    socket.send(len(data).to_bytes(4,  'big'))
    socket.send(data)


def recv(sockSrc, addrSrc, addrDst, from_t):
    data = None
    if from_t == "udp":
        data, newAddrSrc = sockSrc.recvfrom(1024)
        if not newAddrSrc:
            return None
    elif from_t == "tcp":
        data = sockSrc.recv(1024)
    elif from_t == "seg_tcp":
        data = recvSegTCP(sockSrc)
    return data


def send(sockDst, data, sddrSrc, addrDst, to_t):
    #print("{}: {}:{} -> {}:{} = [{}]".format(datetime.datetime.now(),
    #                                    addrSrc[0], addrSrc[1],
    #                                    addrDst[0], addrDst[1],
    #                                    data))
    if to_t == "udp":
        sockDst.sendto(data, addrDst)
    elif to_t == "tcp":
        sockDst.send(data)
    elif to_t == "seg_tcp":
        sendSegTCP(sockDst, data)


def startConnexions(sockSrc, addrSrc):
    global NEXT_UDP_CONNEXION_PORT
    sockDst = None
    if ARGS.to == "udp":
        sockDst = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sockDst.bind(("0.0.0.0", NEXT_UDP_CONNEXION_PORT))
        NEXT_UDP_CONNEXION_PORT += 1
    elif ARGS.to == "tcp" or ARGS.to == "seg_tcp":
        sockDst = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sockDst.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        sockDst.connect((ARGS.server, ARGS.dport))

    print("Starting connexion relay from {}:{}->{}:{} to {}:{}->{}:{}"
            .format(addrSrc[0],
                    addrSrc[1],
                    sockSrc.getsockname()[0],
                    sockSrc.getsockname()[1],
                    sockDst.getsockname()[0],
                    sockDst.getsockname()[1],
                    ARGS.server,
                    ARGS.dport)
          )
    if ARGS._from == "udp":
        SOCKETS_FROM_ADDR[addrSrc] = sockDst
    elif ARGS._from == "tcp" or ARGS._from == "seg_tcp":
        fowThread = Thread(target=relay, args=[sockSrc, sockDst, addrSrc,
                            (ARGS.server, ARGS.dport), ARGS._from, ARGS.to])
        fowThread.start()

    print("Starting reverse connexion relay from {}:{}->{}:{} to {}:{}->{}:{}"
            .format(ARGS.server,
                    ARGS.dport,
                    sockDst.getsockname()[0],
                    sockDst.getsockname()[1],
                    sockSrc.getsockname()[0],
                    sockSrc.getsockname()[1],
                    addrSrc[0],
                    addrSrc[1])
          )
    revThread = Thread(target=relay, args=[sockDst, sockSrc,
                    (ARGS.server, ARGS.dport), addrSrc, ARGS.to, ARGS._from])
    revThread.start()


def relay(sockSrc, sockDst, addrSrc, addrDst, from_t, to_t):
    #print("Starting Relay from {}:{} to {}:{}"
    #    .format(addrSrc[0], addrSrc[1], addrDst[0], addrDst[1]))
    while True:
        data = recv(sockSrc, addrSrc, addrDst, from_t)
        if not data:
            print("BROKEN PIPE: Closing connexion relay from {}:{}->{}:{} to {}:{}->{}:{}"
                    .format(addrSrc[0],
                            addrSrc[1],
                            sockSrc.getsockname()[0],
                            sockSrc.getsockname()[1],
                            sockDst.getsockname()[0],
                            sockDst.getsockname()[1],
                            addrDst[0],
                            addrDst[1])
                  )
            sockSrc.close()
            sockDst.close()
            return
        send(sockDst, data, addrSrc, addrDst, to_t)


def mainUDP():
    sockServer = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sockServer.bind((ARGS.listen, ARGS.port))
    while True:
        data, addrSrc = sockServer.recvfrom(1024)
        if not addrSrc or not data:
            continue
        if addrSrc not in SOCKETS_FROM_ADDR:
            startConnexions(sockServer, addrSrc)
        sockDst = SOCKETS_FROM_ADDR[addrSrc]
        send(sockDst, data, addrSrc, (ARGS.server, ARGS.dport), ARGS.to)


def mainTCP():
    sockServer = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sockServer.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
    sockServer.bind((ARGS.listen, ARGS.port))
    sockServer.listen(1)
    while True:
        sockSrc, addrSrc = sockServer.accept()
        startConnexions(sockSrc, addrSrc)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
                                prog='UDPonTCP',
                                description='Provide a client/server stup '
                                'for Tunneling UDP connexions through TCP.',
                                epilog='UDPonTCP by Xarus')

    #parser.add_argument('-m', '--mod', choices=['server', 'client'],
    #                    help="Specify Server or Client Mod", required = True)
    parser.add_argument('-f', '--from', choices=['udp', 'tcp', 'seg_tcp'],
                        help="Convert from UDP, TCP or segmented TCP protocol",
                        required=True, dest="_from")
    parser.add_argument('-t', '--to', choices=['udp', 'tcp', 'seg_tcp'],
                        help="Convert to UDP, TCO or segmented TCP protocol",
                        required=True)
    parser.add_argument('-l', '--listen',
                        help="Addres to listen on.", required=True)
    parser.add_argument('-p', '--port', type=int,
                        help="Port to listen on.", required=True)
    parser.add_argument('-s', '--server',
                        help="Server Addres to connect to.", required=True)
    parser.add_argument('-q', '--dport', type=int,
                        help="Server port to connect to.", required=True)
    parser.add_argument('--port-start-from', type=int, default=50000,
                        help="At witch port do the server "
                        "start to use to connect to the final target. "
                        "Is incremented by one at each connexion."
                        "Only make sens if --to is udp",
                        required = False)

    ARGS = parser.parse_args()
    print(ARGS)
    SOCKETS_FROM_ADDR = {}
    NEXT_UDP_CONNEXION_PORT = ARGS.port_start_from
    if ARGS._from == "udp":
        mainUDP()
    elif ARGS._from == "tcp" or ARGS._from == "seg_tcp":
        mainTCP()

    exit(0)

