

UDP\_TCP\_relay.py is a tool to relay UDP/TCP connexions other UDP/TCP connxions.

# Usage Exemple

You have a factorio server running on host `exampl.com` and port `34197` in `udp`.
You try to connect to it trough cellular network and the lake of reliability of udp make the connexion consistently failed.

By running the following command on your local computer, you can transfer incomming connexion on `localhost` in `udp` with port `34197` to a `tcp` connxion on `example.com` with port `34198`.

Here `seg_tcp` indicate to the programme that tcp should not act as a stream and should not merge packets as a standard tcp connexion. Merging packets will prevent further deduplication of udp packet on server.
```
./UDP_TCP_relay.py --from udp --to seg_tcp --listen 127.0.0.1 --port 34197 --server example.com --dport 34198
```

Then, by running the following commend on your server, you can transfer incomming connexion on `0.0.0.0` in `tcp` with port `34198` to a `udp` connexion on `localhost` with port `34197`.
```
./UDP_TCP_relay.py --from seg_tcp --to udp --listen 0.0.0.0 --port 34198 --server localhost --dport 34197
```
In that whay, you can know connect to your server on `example.com` with port `34197` by connecting to `localhost` with port `34197` and the connexion will be forward to the server in `tcp`.

Please note that the server side should be started first.

There is currently no error checking on socket managment.
Use at your own risk.

